<?php

declare(strict_types=1);

namespace VijoniTest\Unit\Config;

use Vijoni\Config\Exception\CircularReferenceException;
use Vijoni\Config\Exception\FileNotFoundException;
use Vijoni\Config\TemplatesLocalizator;
use Vijoni\Config\Parser\YamlConfigParser;

class YamlTemplatesLocalizatorTest extends \Codeception\Test\Unit
{
  private const CORRECT_SETUP_DIR = __DIR__ . '/fixtures/environments';
  private const FAILING_SETUP_DIR = __DIR__ . '/fixtures/environments_failing';

  public function testFindTemplates(): void
  {
    $configsDir = self::CORRECT_SETUP_DIR;
    $configFilePath = $configsDir . '/dev/config.yaml';

    $templatesLocalizator = $this->newYamlTemplatesLocalizator();
    $templatesPaths = $templatesLocalizator->findTemplatesPaths($configFilePath);

    $this->assertEquals(
      [
        $configsDir . '/default.yaml',
        $configsDir . '/_shared/experiments.yaml',
        $configsDir . '/_shared/test.yaml',
        $configsDir . '/dev/config.yaml',
      ],
      $templatesPaths
    );
  }

  /**
   * @dataProvider fileNotFoundCases
   */
  public function testFindTemplatesGivenFileNotFound(string $configFilePath): void
  {
    $this->expectException(FileNotFoundException::class);
    $templatesLocalizator = $this->newYamlTemplatesLocalizator();
    $templatesLocalizator->findTemplatesPaths($configFilePath);
  }

  public function fileNotFoundCases(): array
  {
    $configsDir = self::FAILING_SETUP_DIR;

    return [
      'Config file' => [$configsDir . '/config_notfound.yaml'],
      '1st level missing file' => [$configsDir . '/file_not_found_config_1st.yaml'],
      '2nd level missing file' => [$configsDir . '/file_not_found_config_2nd.yaml'],
    ];
  }

  /**
   * @dataProvider circularReferenceCases
   */
  public function testFindTemplatesGivenCircularReference(string $configFilePath): void
  {
    $this->expectException(CircularReferenceException::class);
    $templatesLocalizator = $this->newYamlTemplatesLocalizator();
    $templatesLocalizator->findTemplatesPaths($configFilePath);
  }

  public function circularReferenceCases(): array
  {
    $configsDir = self::FAILING_SETUP_DIR;

    return [
      '1st level circular reference' => [$configsDir . '/circular_config_1st.yaml'],
      '2nd level circular reference' => [$configsDir . '/circular_config_2nd.yaml'],
    ];
  }

  private function newYamlTemplatesLocalizator(): TemplatesLocalizator
  {
    $configParser = new YamlConfigParser();

    return new TemplatesLocalizator($configParser);
  }
}
