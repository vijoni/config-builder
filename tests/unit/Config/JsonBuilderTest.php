<?php

declare(strict_types=1);

namespace VijoniTest\Unit\Config;

use Vijoni\Config\Builder;
use Vijoni\Config\Parser\JsonConfigParser;

class JsonBuilderTest extends \Codeception\Test\Unit
{
  private const FIXTURES_DIR = __DIR__ . '/fixtures';

  public function testBuildConfig(): void
  {
    $configBuilder = $this->newJsonBuilder();
    $config = $configBuilder->build(
      [
        self::FIXTURES_DIR . '/config-template.json',
      ],
      $this->getSampleTemplateVariables()
    );

    $this->assertEquals(
      file_get_contents(self::FIXTURES_DIR . '/config-expected.php'),
      $config
    );
  }

  public function testBuildFromMergedTemplates(): void
  {
    $environmentsDir = self::FIXTURES_DIR . '/environments';

    $configBuilder = $this->newJsonBuilder();
    $config = $configBuilder->build(
      [
        $environmentsDir . '/default.json',
        $environmentsDir . '/_shared/experiments.json',
        $environmentsDir . '/_shared/test.json',
        $environmentsDir . '/dev/config.json',
      ],
      $this->getSampleTemplateVariables()
    );

    $this->assertEquals(
      file_get_contents(self::FIXTURES_DIR . '/config-expected-merged-dev.php'),
      $config
    );
  }

  public function testBuildConfigGivenEnvVars(): void
  {
    $this->setSampleEnvVars();

    $configBuilder = $this->newJsonBuilder();
    $config = $configBuilder->build(
      [
        self::FIXTURES_DIR . '/config-template.json',
      ],
      getenv()
    );

    $this->assertEquals(
      file_get_contents(self::FIXTURES_DIR . '/config-expected.php'),
      $config
    );
  }

  private function newJsonBuilder(): Builder
  {
      $configParser = new JsonConfigParser();

      return new Builder($configParser);
  }

  private function getSampleTemplateVariables(): array
  {
    return [
      'DATABASE_HOST' => 'localhost',
      'DATABASE_PASS' => 'fao356j',
      'DATABASE_USER' => 'admin',
      'DATABASE_NAME' => 'vijoni',
    ];
  }

  private function setSampleEnvVars(): void
  {
      putenv('DATABASE_HOST=localhost');
      putenv('DATABASE_PASS=fao356j');
      putenv('DATABASE_USER=admin');
      putenv('DATABASE_NAME=vijoni');
  }
}
