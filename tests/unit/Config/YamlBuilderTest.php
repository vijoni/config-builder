<?php

declare(strict_types=1);

namespace VijoniTest\Unit\Config;

use Vijoni\Config\Builder;
use Vijoni\Config\Parser\YamlConfigParser;

class YamlBuilderTest extends \Codeception\Test\Unit
{
  private const FIXTURES_DIR = __DIR__ . '/fixtures';

  public function testBuildConfig(): void
  {
    $configBuilder = $this->newYamlBuilder();
    $config = $configBuilder->build(
      [
        self::FIXTURES_DIR . '/config-template.yaml',
      ],
      $this->getSampleTemplateVariables()
    );

    $this->assertEquals(
      file_get_contents(self::FIXTURES_DIR . '/config-expected.php'),
      $config
    );
  }

  public function testBuildFromMergedTemplates(): void
  {
    $environmentsDir = self::FIXTURES_DIR . '/environments';

    $configBuilder = $this->newYamlBuilder();
    $config = $configBuilder->build(
      [
        $environmentsDir . '/default.yaml',
        $environmentsDir . '/_shared/experiments.yaml',
        $environmentsDir . '/_shared/test.yaml',
        $environmentsDir . '/dev/config.yaml',
      ],
      $this->getSampleTemplateVariables()
    );

    $this->assertEquals(
      file_get_contents(self::FIXTURES_DIR . '/config-expected-merged-dev.php'),
      $config
    );
  }

  public function testBuildConfigGivenEnvVars(): void
  {
    $this->setSampleEnvVars();

    $configBuilder = $this->newYamlBuilder();
    $config = $configBuilder->build(
      [
        self::FIXTURES_DIR . '/config-template.yaml',
      ],
      getenv()
    );

    $this->assertEquals(
      file_get_contents(self::FIXTURES_DIR . '/config-expected.php'),
      $config
    );
  }

  private function newYamlBuilder(): Builder
  {
      $configParser = new YamlConfigParser();

      return new Builder($configParser);
  }

  private function getSampleTemplateVariables(): array
  {
    return [
      'DATABASE_HOST' => 'localhost',
      'DATABASE_PASS' => 'fao356j',
      'DATABASE_USER' => 'admin',
      'DATABASE_NAME' => 'vijoni',
    ];
  }

  private function setSampleEnvVars(): void
  {
      putenv('DATABASE_HOST=localhost');
      putenv('DATABASE_PASS=fao356j');
      putenv('DATABASE_USER=admin');
      putenv('DATABASE_NAME=vijoni');
  }
}
