<?php

declare(strict_types=1);

namespace VijoniTest\Unit\Config\Parser;

use Vijoni\Config\Parser\JsonConfigParser;

class JsonConfigParserTest extends \Codeception\Test\Unit
{
  public function testToArray(): void
  {
    $sampleYamlContent = $this->getSampleJsonContent();
    $configParser = new JsonConfigParser();
    $config = $configParser->toArray($sampleYamlContent);

    $this->assertEquals(
      [
        'config' => [
          'database' => [
            'host' => 'localhost',
            'name' => 'sample_database',
          ]
        ]
      ],
      $config
    );
  }

  private function getSampleJsonContent(): string
  {
    return <<<JSON
{
"config": {
  "database": {
      "host": "localhost",
      "name": "sample_database"
    }
  }
}
JSON;
  }
}
