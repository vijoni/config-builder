<?php

declare(strict_types=1);

namespace VijoniTest\Unit\Config\Parser;

use Vijoni\Config\Parser\YamlConfigParser;

class YamlConfigParserTest extends \Codeception\Test\Unit
{
  public function testToArray(): void
  {
    $sampleYamlContent = $this->getSampleYamlContent();
    $configParser = new YamlConfigParser();
    $config = $configParser->toArray($sampleYamlContent);

    $this->assertEquals(
      [
        'config' => [
          'database' => [
            'host' => 'localhost',
            'name' => 'sample_database',
          ]
        ]
      ],
      $config
    );
  }

  private function getSampleYamlContent(): string
  {
    return <<<YAML
---
config:
  database:
    host: localhost
    name: sample_database
YAML;
  }
}
