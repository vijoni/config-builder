<?php

declare(strict_types=1);

namespace VijoniTest\Unit\Config\Parser;

use Vijoni\Config\Parser\PhpConfigParser;

class PhpConfigParserTest extends \Codeception\Test\Unit
{
  public function testToArray(): void
  {
    $sampleYamlContent = $this->getSamplePhpContent();
    $configParser = new PhpConfigParser();
    $config = $configParser->toArray($sampleYamlContent);

    $this->assertEquals(
      [
        'config' => [
          'database' => [
            'host' => 'localhost',
            'name' => 'sample_database',
          ]
        ]
      ],
      $config
    );
  }

  private function getSamplePhpContent(): string
  {
    return <<<PHP
return [
'config' => [
  'database' => [
      'host' => 'localhost',
      'name' => 'sample_database',
    ]
  ]
];
PHP;
  }
}
