<?php

declare(strict_types=1);

namespace VijoniTest\Unit\Config;

use Vijoni\Config\Parser\EncryptedConfigParser;
use Vijoni\Config\Exception\GitCryptMissingValidKeyFileException;
use Vijoni\Config\GitCrypt;
use Vijoni\Config\Parser\YamlConfigParser;

class YamlEncryptedConfigParserTest extends \Codeception\Test\Unit
{
  private const KEY_FILE_PATH = __DIR__ . '/support/git-crypt.key';
  private const KEY_XYZ_FILE_PATH = __DIR__ . '/support/git-crypt-xyz.key';

  /**
   * @dataProvider provideAvailableGitCryptKeyFilePaths
   */
  public function testToArray(array $availableGitCryptKeyFilePaths): void
  {
    $sampleYamlContent = $this->getSampleYamlContent();

    $gitCrypt = new GitCrypt();
    $encryptedContent = $gitCrypt->encrypt($sampleYamlContent, self::KEY_FILE_PATH);

    $configParser = new EncryptedConfigParser(
      new YamlConfigParser(),
      $availableGitCryptKeyFilePaths,
      $gitCrypt
    );
    $config = $configParser->toArray($encryptedContent);

    $this->assertEquals(
      [
        'config' => [
          'database' => [
            'host' => 'localhost',
            'name' => 'sample_database',
          ]
        ]
      ],
      $config
    );
  }

  public function provideAvailableGitCryptKeyFilePaths(): array
  {
    return [
      'single valid key file' => [
        [self::KEY_FILE_PATH]
      ],
      'any valid key file' => [
        [self::KEY_XYZ_FILE_PATH, self::KEY_FILE_PATH]
      ],
    ];
  }

  public function testToArrayGivenWrongGitCryptKey(): void
  {
    $sampleYamlContent = $this->getSampleYamlContent();

    $gitCrypt = new GitCrypt();
    $encryptedContent = $gitCrypt->encrypt($sampleYamlContent, self::KEY_FILE_PATH);

    $this->expectException(GitCryptMissingValidKeyFileException::class);
    $configParser = new EncryptedConfigParser(new YamlConfigParser(), [self::KEY_XYZ_FILE_PATH], $gitCrypt);
    $configParser->toArray($encryptedContent);
  }

  private function getSampleYamlContent(): string
  {
    return <<<YAML
---
config:
  database:
    host: localhost
    name: sample_database
YAML;
  }
}
