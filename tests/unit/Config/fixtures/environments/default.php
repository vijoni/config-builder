<?php

return [
  'config' => [
    'enable_checkout_payment_experiment' => true,
    'database' => [
      'host' => '${DATABASE_HOST}',
      'pass' => '${DATABASE_PASS}',
      'user' => '${DATABASE_USER}',
      'name' => '${DATABASE_NAME}'
    ],
  ],
];
