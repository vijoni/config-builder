<?php

return [
  'config' => [
    'database' => [
      'host' => '${DATABASE_HOST}',
      'pass' => '${DATABASE_PASS}',
      'user' => '${DATABASE_USER}',
      'name' => '${DATABASE_NAME}'
    ]
  ]
];
