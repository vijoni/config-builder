<?php

declare(strict_types=1);

namespace VijoniTest\Unit\Config;

use Vijoni\Config\Exception\GitCryptCommandException;
use Vijoni\Config\GitCrypt;

class GitCryptTest extends \Codeception\Test\Unit
{
  public function testEncryptDecryptContentGivenBase64EncodedKey(): void
  {
    $outputDir = codecept_output_dir('Config');
    @mkdir($outputDir, 0777, true);

    $keyFilePath = "{$outputDir}/git-crypt.key";
    $sampleContent = 'Lorem ipsum dolor sit amet';

    $gitCrypt = new GitCrypt();
    $gitCrypt->createKeyFileFromB64Key(
      $this->sampleBase64EncodedKey(),
      $keyFilePath
    );

    $encryptedContent = $gitCrypt->encrypt($sampleContent, $keyFilePath);
    $decryptedContent = $gitCrypt->decrypt($encryptedContent, $keyFilePath);

    $this->assertEquals($sampleContent, $decryptedContent);
  }

  public function testDecryptContentGivenWrongKeyFile(): void
  {
    $sampleContent = 'Lorem ipsum dolor sit amet';
    $correctKeyFilePath = __DIR__ . '/support/git-crypt.key';
    $wrongKeyFilePath = __DIR__ . '/support/git-crypt-xyz.key';

    $this->expectException(GitCryptCommandException::class);

    $gitCrypt = new GitCrypt();
    $encryptedContent = $gitCrypt->encrypt($sampleContent, $correctKeyFilePath);
    $decryptedContent = $gitCrypt->decrypt($encryptedContent, $wrongKeyFilePath);

    $this->assertEquals($sampleContent, $decryptedContent);
  }

  private function sampleBase64EncodedKey(): string
  {
    // phpcs:ignore Generic.Files.LineLength
    return 'AEdJVENSWVBUS0VZAAAAAgAAAAAAAAABAAAABAAAAAAAAAADAAAAIItUAkEy0p5mr5LoDemHYHod+FFa8xMfJM42JZwJl6AgAAAABQAAAEAnfTYw1diKOstDxhXZ2r2VH3QG0tfVOs5PgWELKrgNchEf9ZxDeGl0ATTqzl1IWi400AJHdOJMKekrZyn9H5xzAAAAAA==';
  }
}
