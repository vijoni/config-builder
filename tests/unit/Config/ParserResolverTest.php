<?php

declare(strict_types=1);

namespace VijoniTest\Unit\Config;

use Vijoni\Config\GitCrypt;
use Vijoni\Config\Parser\EncryptedConfigParser;
use Vijoni\Config\Parser\JsonConfigParser;
use Vijoni\Config\Parser\PhpConfigParser;
use Vijoni\Config\Parser\YamlConfigParser;
use Vijoni\Config\ParserResolver;

class ParserResolverTest extends \Codeception\Test\Unit
{
  /**
   * @dataProvider parserResolverCases
   */
  public function testParserResolver(string $filePath, string $expectedResolverClass): void
  {
    $parserResolver = new ParserResolver();
    $parser = $parserResolver->resolve($filePath);

    $this->assertInstanceOf($expectedResolverClass, $parser); /** @phpstan-ignore-line */
  }

  public function testParserResolverEncrypted(): void
  {
    $filePath = '../environments/config.yaml';
    $gitCryptMock = $this->createMock(GitCrypt::class);

    $parserResolver = new ParserResolver();
    $parser = $parserResolver->resolveEncrypted($filePath, [], $gitCryptMock);

    $this->assertInstanceOf(EncryptedConfigParser::class, $parser);
  }

  public function parserResolverCases(): array
  {
    return [
      'yaml' => [
        '../environments/config.yaml',
        YamlConfigParser::class,
      ],
      'yml' => [
        '../environments/config.yml',
        YamlConfigParser::class,
      ],
      'json' => [
        '../environments/config.json',
        JsonConfigParser::class,
      ],
      'php' => [
        '../environments/config.php',
        PhpConfigParser::class,
      ],
    ];
  }
}
