<?php

declare(strict_types=1);

namespace VijoniTest\Unit\Config;

use Vijoni\Config\Builder;
use Vijoni\Config\Parser\PhpConfigParser;

class PhpBuilderTest extends \Codeception\Test\Unit
{
  private const FIXTURES_DIR = __DIR__ . '/fixtures';

  public function testBuildConfig(): void
  {
    $configBuilder = $this->newPhpBuilder();
    $config = $configBuilder->build(
      [
        self::FIXTURES_DIR . '/config-template.php',
      ],
      $this->getSampleTemplateVariables()
    );

    $this->assertEquals(
      file_get_contents(self::FIXTURES_DIR . '/config-expected.php'),
      $config
    );
  }

  public function testBuildFromMergedTemplates(): void
  {
    $environmentsDir = self::FIXTURES_DIR . '/environments';

    $configBuilder = $this->newPhpBuilder();
    $config = $configBuilder->build(
      [
        $environmentsDir . '/default.php',
        $environmentsDir . '/_shared/experiments.php',
        $environmentsDir . '/_shared/test.php',
        $environmentsDir . '/dev/config.php',
      ],
      $this->getSampleTemplateVariables()
    );

    $this->assertEquals(
      file_get_contents(self::FIXTURES_DIR . '/config-expected-merged-dev.php'),
      $config
    );
  }

  public function testBuildConfigGivenEnvVars(): void
  {
    $this->setSampleEnvVars();

    $configBuilder = $this->newPhpBuilder();
    $config = $configBuilder->build(
      [
        self::FIXTURES_DIR . '/config-template.php',
      ],
      getenv()
    );

    $this->assertEquals(
      file_get_contents(self::FIXTURES_DIR . '/config-expected.php'),
      $config
    );
  }

  private function newPhpBuilder(): Builder
  {
      $configParser = new PhpConfigParser();

      return new Builder($configParser);
  }

  private function getSampleTemplateVariables(): array
  {
    return [
      'DATABASE_HOST' => 'localhost',
      'DATABASE_PASS' => 'fao356j',
      'DATABASE_USER' => 'admin',
      'DATABASE_NAME' => 'vijoni',
    ];
  }

  private function setSampleEnvVars(): void
  {
      putenv('DATABASE_HOST=localhost');
      putenv('DATABASE_PASS=fao356j');
      putenv('DATABASE_USER=admin');
      putenv('DATABASE_NAME=vijoni');
  }
}
