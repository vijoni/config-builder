<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Config;

use Vijoni\Config\Parser\EncryptedConfigParser;
use Vijoni\Config\Builder;
use Vijoni\Config\GitCrypt;
use Vijoni\Config\TemplatesLocalizator;
use Vijoni\Config\Parser\YamlConfigParser;

class YamlBuilderTest extends \Codeception\Test\Unit
{
  private const FIXTURES_DIR = __DIR__ . '/fixtures';

  public function testGenerateConfig(): void
  {
    $this->setSampleEnvVars();

    $configFilePath = self::FIXTURES_DIR . '/environments/dev/config.yaml';

    $configParser = new YamlConfigParser();
    $templatesLocalizator = new TemplatesLocalizator($configParser);
    $templatesPaths = $templatesLocalizator->findTemplatesPaths($configFilePath);
    $configGenerator = new Builder($configParser);

    $generatedConfig = $configGenerator->build($templatesPaths, getenv());

    $this->assertEquals(
      file_get_contents(self::FIXTURES_DIR . '/config-expected-merged-dev.php'),
      $generatedConfig
    );
  }

  public function testGenerateConfigGivenEncryptedFiles(): void
  {
    $this->setSampleEnvVars();

    $configFilePath = self::FIXTURES_DIR . '/environments_encrypted/prod/config.yaml';

    $gitCrypt = new GitCrypt();

    $yamlConfigParser = new YamlConfigParser();
    $configParser = new EncryptedConfigParser(
      $yamlConfigParser,
      [
        __DIR__ . '/support/git-crypt-default.key',
        __DIR__ . '/support/git-crypt-prod.key',
      ],
      $gitCrypt
    );
    $templatesLocalizator = new TemplatesLocalizator($configParser);
    $templatesPaths = $templatesLocalizator->findTemplatesPaths($configFilePath);
    $configGenerator = new Builder($configParser);

    $generatedConfig = $configGenerator->build($templatesPaths, getenv());

    $this->assertEquals(
      file_get_contents(self::FIXTURES_DIR . '/config-expected-merged-prod.php'),
      $generatedConfig
    );
  }

  private function setSampleEnvVars(): void
  {
      putenv('DATABASE_HOST=localhost');
      putenv('DATABASE_PASS=fao356j');
      putenv('DATABASE_USER=admin');
      putenv('DATABASE_NAME=vijoni');
  }
}
