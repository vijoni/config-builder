<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Config;

use Vijoni\Config\Builder;
use Vijoni\Config\GitCrypt;
use Vijoni\Config\Parser\EncryptedConfigParser;
use Vijoni\Config\TemplatesLocalizator;
use Vijoni\Config\Parser\JsonConfigParser;

class JsonBuilderTest extends \Codeception\Test\Unit
{
  private const FIXTURES_DIR = __DIR__ . '/fixtures';

  public function testGenerateConfig(): void
  {
    $this->setSampleEnvVars();

    $configFilePath = self::FIXTURES_DIR . '/environments/dev/config.json';

    $configParser = new JsonConfigParser();
    $templatesLocalizator = new TemplatesLocalizator($configParser);
    $templatesPaths = $templatesLocalizator->findTemplatesPaths($configFilePath);
    $configGenerator = new Builder($configParser);

    $generatedConfig = $configGenerator->build($templatesPaths, getenv());

    $this->assertEquals(
      file_get_contents(self::FIXTURES_DIR . '/config-expected-merged-dev.php'),
      $generatedConfig
    );
  }

  private function setSampleEnvVars(): void
  {
      putenv('DATABASE_HOST=localhost');
      putenv('DATABASE_PASS=fao356j');
      putenv('DATABASE_USER=admin');
      putenv('DATABASE_NAME=vijoni');
  }
}
