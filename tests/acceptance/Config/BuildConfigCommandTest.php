<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Config;

class BuildConfigCommandTest extends \Codeception\Test\Unit
{
  private const SUPPORT_DIR = __DIR__ . '/support';
  private const FIXTURES_DIR = __DIR__ . '/fixtures';

  private string $outputDir = '';

  protected function _before(): void
  {
    $outputDir = codecept_output_dir('Config/Command');
    @mkdir($outputDir, 0777, true);
    @unlink("{$outputDir}/config.php");

    $this->outputDir = $outputDir;
  }

  public function testBuildConfig(): void
  {
    $environmentsDir = self::FIXTURES_DIR . '/environments';
    $script = PROJECT_DIR . 'bin/build-config';

    $cmd = <<<CMD
{$script} \
-c {$environmentsDir}/prod/config.yaml \
-o {$this->outputDir}/config.php
CMD;

    $this->setSampleEnvVars();
    shell_exec($cmd);

    $this->assertEquals(
      file_get_contents(self::FIXTURES_DIR . '/config-expected-merged-prod.php'),
      file_get_contents($this->outputDir . '/config.php')
    );
  }

  public function testBuildConfigGivenGitCryptKeyFiles(): void
  {
    $supportDir = self::SUPPORT_DIR;
    $environmentsDir = self::FIXTURES_DIR . '/environments_encrypted';
    $script = PROJECT_DIR . 'bin/build-config';

    $cmd = <<<CMD
{$script} \
-k {$supportDir}/git-crypt-default.key \
-k {$supportDir}/git-crypt-prod.key \
-c {$environmentsDir}/prod/config.yaml \
-o {$this->outputDir}/config.php
CMD;

    $this->setSampleEnvVars();
    shell_exec($cmd);

    $this->assertEquals(
      file_get_contents(self::FIXTURES_DIR . '/config-expected-merged-prod.php'),
      file_get_contents($this->outputDir . '/config.php')
    );
  }

  private function setSampleEnvVars(): void
  {
      putenv('DATABASE_HOST=localhost');
      putenv('DATABASE_PASS=fao356j');
      putenv('DATABASE_USER=admin');
      putenv('DATABASE_NAME=vijoni');
  }
}
