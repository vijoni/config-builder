<?php

declare(strict_types=1);

namespace VijoniTest\Acceptance\Config;

class DecodeGcKeyCommandTest extends \Codeception\Test\Unit
{
  public function testCreateGitCryptKeyFileGivenBase64EncodedKey(): void
  {
    $sampleContent = 'Lorem ipsum dolor sit amet';
    $dir = codecept_output_dir('Config/Command');
    @mkdir($dir, 0777, true);

    $script = PROJECT_DIR . 'bin/decode-gckey';

    // phpcs:disable Generic.Files.LineLength
    shell_exec("echo {$this->sampleBase64EncodedKey()} | {$script} -o {$dir}/git-crypt.key");
    shell_exec("cat {$dir}/sample_content | git-crypt clean --key-file={$dir}/git-crypt.key | tee {$dir}/encrypted_content > /dev/null");
    $decryptedContent = shell_exec("cat {$dir}/encrypted_content | git-crypt smudge --key-file={$dir}/git-crypt.key");
    // phpcs:enable

    $this->assertEquals($sampleContent, $decryptedContent);
  }

  private function sampleBase64EncodedKey(): string
  {
    // phpcs:ignore Generic.Files.LineLength
    return 'AEdJVENSWVBUS0VZAAAAAgAAAAAAAAABAAAABAAAAAAAAAADAAAAIItUAkEy0p5mr5LoDemHYHod+FFa8xMfJM42JZwJl6AgAAAABQAAAEAnfTYw1diKOstDxhXZ2r2VH3QG0tfVOs5PgWELKrgNchEf9ZxDeGl0ATTqzl1IWi400AJHdOJMKekrZyn9H5xzAAAAAA==';
  }
}
