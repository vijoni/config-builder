<?php

return [
  'extends' => [
    './experiments.php',
  ],
  'config' => [
    'enable_checkout_payment_experiment' => true,
  ],
];
