#Config Builder Library

#### Build a php environment specific configuration file based on organized template config files.

## Usage
#### Check acceptance tests for more details

Sample file structure:

![](doc/img/directory-structure.png)

Sample configuration file contents (not encrypted)

``` yaml
// default.yaml
---
config:
  enable_checkout_payment_experiment: true
  database:
    host: ${DATABASE_HOST}
    pass: ${DATABASE_PASS}
    user: ${DATABASE_USER}
    name: ${DATABASE_NAME}

// prod/config.yaml
---
extends:
- '../default.yaml'
- '../_shared/experiments.yaml'

config:
```

Build a key file from a base64 encoded git-crypt file contents provided as environment variable:
``` shell
echo $DEFAULT_GC_KEY | bin/decode-gckey -o /tmp/git-crypt-default.key
echo $PROD_GC_KEY | bin/decode-gckey -o /tmp/git-crypt-prod.key
```

Build configuration file.

If `-k` argument is omitted script will not use decryption.  \
Command accepts multiple git-crypt key file paths.  \
If none of the provided key files is able to decrypt a file, command will exit with an error.  \
The command reads environment variables to replace template's placeholder literals.

``` shell
bin/build-config \
-k /tmp/git-crypt-default.key \
-k /tmp//git-crypt-prod.key \
-c environments/prod/config.yaml \
-o config/config.php
```

### Templates
Templates can contain placeholder literals `${VAR_NAME}`,
which during the build process will be replaced with corresponding values.

You can organize your configuration in multiple files.
Use the `extends` key to specify additional files.
The files on the bottom of the list will override the values provided in the files above.
Values provided in the `config` key will override the values coming from extended files.

YAML, JSON, PHP templates are supported. Check acceptance tests for sample configuration files.
