<?php

declare(strict_types=1);

namespace Vijoni\Config\Exception;

class FileNotFoundException extends \Exception
{
}
