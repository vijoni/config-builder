<?php

declare(strict_types=1);

namespace Vijoni\Config\Exception;

use Throwable;

class ConfigParserException extends \Exception
{
  public function __construct(string $errorMessage, ?Throwable $previous = null)
  {
    parent::__construct($errorMessage, 0, $previous);
  }
}
