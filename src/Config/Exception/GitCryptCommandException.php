<?php

declare(strict_types=1);

namespace Vijoni\Config\Exception;

class GitCryptCommandException extends \Exception
{
  public function __construct(string $errorMessage)
  {
    parent::__construct($errorMessage);
  }
}
