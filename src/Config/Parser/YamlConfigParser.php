<?php

declare(strict_types=1);

namespace Vijoni\Config\Parser;

use Vijoni\Config\Exception\ConfigParserException;

class YamlConfigParser implements ConfigParser
{
  public function toArray(string $content): array
  {
    if (empty($content)) {
      return [];
    }

    $result = yaml_parse($content);
    $result === false && throw new ConfigParserException("Invalid content:\n{$content}");

    return (array)$result;
  }
}
