<?php

declare(strict_types=1);

namespace Vijoni\Config\Parser;

use Vijoni\Config\Exception\ConfigParserException;

class JsonConfigParser implements ConfigParser
{
  public function toArray(string $content): array
  {
    if (empty($content)) {
      return [];
    }

    $result = json_decode($content, true);
    $result === null && throw new ConfigParserException("Invalid content:\n{$content}");

    return (array)$result;
  }
}
