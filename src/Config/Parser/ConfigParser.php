<?php

declare(strict_types=1);

namespace Vijoni\Config\Parser;

interface ConfigParser
{
  public function toArray(string $content): array;
}
