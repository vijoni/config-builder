<?php

declare(strict_types=1);

namespace Vijoni\Config\Parser;

use Vijoni\Config\Exception\ConfigParserException;

class PhpConfigParser implements ConfigParser
{
  public function toArray(string $content): array
  {
    if (empty($content)) {
      return [];
    }

    $cleanedContent = preg_replace('/^<\?php/', '', trim($content));

    try {
      $result = eval($cleanedContent);
      $result === null && throw new ConfigParserException("Invalid content:\n{$content}");
    } catch (\ParseError $e) {
      throw new ConfigParserException("Invalid content:\n{$content}", $e);
    }

    return (array)$result;
  }
}
