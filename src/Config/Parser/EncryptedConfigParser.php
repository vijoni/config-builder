<?php

declare(strict_types=1);

namespace Vijoni\Config\Parser;

use Vijoni\Config\Exception\GitCryptCommandException;
use Vijoni\Config\Exception\GitCryptMissingValidKeyFileException;
use Vijoni\Config\GitCrypt;

class EncryptedConfigParser implements ConfigParser
{
  public function __construct(
    private ConfigParser $configParser,
    private array $availableGitCryptKeyFilePaths,
    private GitCrypt $gitCrypt
  ) {
  }

  /**
   * @throws GitCryptMissingValidKeyFileException
   */
  public function toArray(string $content): array
  {
    $decryptedContent = $this->decryptContent($content);
    return $this->configParser->toArray($decryptedContent);
  }

  /**
   * @throws GitCryptMissingValidKeyFileException
  */
  private function decryptContent(string $content): string
  {
    $errorMessageTemplate = 'file: [%s], error: [%s]';
    $errorMessages = [];

    foreach ($this->availableGitCryptKeyFilePaths as $keyFilePath) {
      try {
        return $this->gitCrypt->decrypt($content, $keyFilePath);
      } catch (GitCryptCommandException $e) {
        $errorMessages[] = sprintf($errorMessageTemplate, $keyFilePath, $e->getMessage());
      }
    }

    if ($errorMessages) {
      $message = "Did not match any of the provided git-crypt key files\n";
      $message .= implode("\n", $errorMessages);

      throw new GitCryptMissingValidKeyFileException($message);
    }

    return '';
  }
}
