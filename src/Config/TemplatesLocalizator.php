<?php

declare(strict_types=1);

namespace Vijoni\Config;

use Vijoni\Config\Exception\CircularReferenceException;
use Vijoni\Config\Exception\FileNotFoundException;
use Vijoni\Config\Parser\ConfigParser;

class TemplatesLocalizator
{
  public function __construct(private ConfigParser $configParser)
  {
  }

  /**
   * @throws FileNotFoundException
   */
  public function findTemplatesPaths(string $configFilePath): array
  {
    $filePath = realpath($configFilePath) ?: '';
    $this->verifyPathEntry($filePath);

    return $this->findParentPaths($filePath, []);
  }

  private function findParentPaths(string $sourceFilePath, array $previousFilePaths): array
  {
    $parentPaths = $this->readParentPaths($sourceFilePath);
    $this->verifyCircularReference($sourceFilePath, $previousFilePaths);

    $result = [];
    $previousFilePaths[] = $sourceFilePath;
    foreach ($parentPaths as $filePath) {
      $result = [
        ...$result,
        ...$this->findParentPaths($filePath, $previousFilePaths),
      ];
    }

    return [
      ...$result,
      $sourceFilePath,
    ];
  }

  /**
   * @throws FileNotFoundException
   */
  private function verifyPathEntry(string $filePath): void
  {
    if (!file_exists($filePath)) {
      throw new FileNotFoundException("File not found [{$filePath}]");
    }
  }

  /**
   * @throws CircularReferenceException
   */
  private function verifyCircularReference(string $filePath, array $paths): void
  {
    if (in_array($filePath, $paths)) {
      throw new CircularReferenceException("File already processed [{$filePath}]");
    }
  }

  private function readParentPaths(string $sourceFilePath): array
  {
    $dir = dirname($sourceFilePath);
    $content = file_get_contents($sourceFilePath);

    if (empty($content)) {
      return [];
    }

    $data = $this->configParser->toArray($content);
    $parentPaths = $data['extends'] ?? [];

    $result = [];
    foreach ($parentPaths as $path) {
      $filePath = realpath("{$dir}/{$path}") ?: '';
      empty($filePath) && throw new FileNotFoundException("File not found [{$dir}/{$path}]");

      $this->verifyPathEntry($filePath);
      $result[] = $filePath;
    }

    return $result;
  }
}
