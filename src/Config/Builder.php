<?php

declare(strict_types=1);

namespace Vijoni\Config;

use Vijoni\Config\Parser\ConfigParser;

class Builder
{
  public function __construct(private ConfigParser $configParser)
  {
  }

  public function build(array $templateFilePaths, array $tplVars): string
  {
    $configArray = [];
    ['placeholders' => $placeholders, 'values' => $values] = $this->createReplaceMap($tplVars);

    foreach ($templateFilePaths as $templateFilePath) {
      $parsedTemplateArray = $this->parseTemplate($templateFilePath, $placeholders, $values);
      $configArray = array_replace_recursive($configArray, $parsedTemplateArray);
    }

    $exportContent = $this->varexportShortSyntax($configArray);

    return $this->warpInPhp($exportContent);
  }

  private function createReplaceMap(array $tplVars): array
  {
    $placeholders = [];
    $values = [];
    foreach ($tplVars as $key => $val) {
      $placeholders[] = "\${{$key}}";
      $values[] = $val;
    }

    return ['placeholders' => $placeholders, 'values' => $values];
  }

  private function parseTemplate(string $templateFilePath, array $placeholders, array $values): array
  {
    $template = file_get_contents($templateFilePath);
    is_string($template) || throw new \RuntimeException("Could not read template contents: [{$templateFilePath}]");

    $replacedContents = str_replace(
      $placeholders,
      $values,
      $template /* @phpstan-ignore-line */
    );

    return $this->readOnlyConfig($this->configParser->toArray($replacedContents));
  }

  private function readOnlyConfig(array $parsedTemplateArray): array
  {
    if (array_key_exists('config', $parsedTemplateArray) === false) {
      return [];
    }

    if (empty($parsedTemplateArray['config'])) {
      return [];
    }

    return $parsedTemplateArray['config'];
  }

  private function varexportShortSyntax(array $input): string
  {
      $export = var_export($input, true);
      $patterns = [
        "/array \(/" => '[',
        "/^([ ]*)\)(,?)$/m" => '$1]$2',
        "/=>[ ]?\n[ ]+\[/" => '=> [',
        "/([ ]*)(\'[^\']+\') => ([\[\'])/" => '$1$2 => $3',
      ];

      $result = preg_replace(array_keys($patterns), array_values($patterns), $export);
      is_string($result) || throw new \RuntimeException('Could not export php using short syntax');

      return $result; /* @phpstan-ignore-line */
  }

  private function warpInPhp(string $content): string
  {
    return <<<PHP
<?php

return {$content};

PHP;
  }
}
