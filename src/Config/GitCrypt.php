<?php

declare(strict_types=1);

namespace Vijoni\Config;

use Vijoni\Config\Exception\GitCryptCommandException;

class GitCrypt
{
  private const ENCRYPT_COMMAND = 'clean';
  private const DECRYPT_COMMAND = 'smudge';

  public function createKeyFileFromB64Key(string $key, string $keyFilePath): void
  {
    $decodedKey = base64_decode($key);
    file_put_contents($keyFilePath, $decodedKey);
  }

  /**
   * @throws GitCryptCommandException
   */
  public function encrypt(string $content, string $keyFilePath): string
  {
    return $this->runEDCommand(self::ENCRYPT_COMMAND, $content, $keyFilePath);
  }

  /**
   * @throws GitCryptCommandException
   */
  public function decrypt(string $content, string $keyFilePath): string
  {
    return $this->runEDCommand(self::DECRYPT_COMMAND, $content, $keyFilePath);
  }

  private function getCommandDescriptorDefinition(): array
  {
    return [
       0 => ['pipe', 'r'],
       1 => ['pipe', 'w'],
       2 => ['pipe', 'w'],
    ];
  }

  private function runEDCommand(string $command, string $stdInContent, string $keyFilePath): string
  {
    $descriptorSpec = $this->getCommandDescriptorDefinition();
    $cmd = "git-crypt {$command} --key-file={$keyFilePath}";

    $proc = proc_open($cmd, $descriptorSpec, $pipes);
    is_resource($proc) || throw new \RuntimeException('Could not run git-crypt command');

    fwrite($pipes[0], $stdInContent);
    fclose($pipes[0]);

    $stdOutContent = stream_get_contents($pipes[1]);
    is_string($stdOutContent) || throw new \RuntimeException('Could not read git-crypt command output');
    fclose($pipes[1]);

    $this->detectError($pipes[2]);

    proc_close($proc); /* @phpstan-ignore-line */

    return $stdOutContent; /* @phpstan-ignore-line */
  }

  /**
   * @param Resource $stream
   * @throws GitCryptCommandException
   */
  private function detectError($stream): void
  {
    $errorMessage = stream_get_contents($stream);
    fclose($stream);

    if (empty($errorMessage)) {
      return;
    }

    if (str_contains($errorMessage, 'file not encrypted')) {
      return;
    }

    throw new GitCryptCommandException($errorMessage);
  }
}
