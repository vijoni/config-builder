<?php

declare(strict_types=1);

namespace Vijoni\Config;

use Vijoni\Config\Exception\UnsupportedConfigParserException;
use Vijoni\Config\Parser\ConfigParser;
use Vijoni\Config\Parser\EncryptedConfigParser;
use Vijoni\Config\Parser\JsonConfigParser;
use Vijoni\Config\Parser\PhpConfigParser;
use Vijoni\Config\Parser\YamlConfigParser;

class ParserResolver
{
  public function resolve(string $filePath): ConfigParser
  {
    $extension = pathinfo($filePath, PATHINFO_EXTENSION);
    $parser = $this->mapFileExtensionToParser($extension);

    return $parser;
  }

  public function resolveEncrypted(
    string $filePath,
    array $availableGitCryptKeyFilePaths,
    GitCrypt $gitCrypt
  ): ConfigParser {
    $parser = $this->resolve($filePath);

    return new EncryptedConfigParser($parser, $availableGitCryptKeyFilePaths, $gitCrypt);
  }

  private function mapFileExtensionToParser(string $extension): ConfigParser
  {
    $map = [
      'yaml' => static fn () => new YamlConfigParser(),
      'yml' => static fn () => new YamlConfigParser(),
      'json' => static fn () => new JsonConfigParser(),
      'php' => static fn () => new PhpConfigParser(),
    ];

    isset($map[$extension]) || throw new UnsupportedConfigParserException($extension);

    return ($map[$extension])();
  }
}
