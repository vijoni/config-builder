<?php

declare(strict_types=1);

// phpcs:ignoreFile
if (!defined('PROJECT_DIR')) {
  define('PROJECT_DIR', __DIR__ . '/../');
}

require_once $_composer_autoload_path ?? __DIR__ . '/../vendor/autoload.php';
